#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDate>
#include <QTime>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void updateTimes();

private:
    Ui::MainWindow *ui;
    QTime getSunTime(QDate dt, double latitude, double longitude, double zenith, qint8 localOffset, bool sunset);


};

#endif // MAINWINDOW_H
