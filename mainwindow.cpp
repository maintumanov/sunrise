#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->dateEdit->setDate(QDate::currentDate());
    connect(ui->comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(updateTimes()));
    connect(ui->doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(updateTimes()));
    connect(ui->doubleSpinBox_2, SIGNAL(valueChanged(double)), this, SLOT(updateTimes()));
    connect(ui->spinBox, SIGNAL(valueChanged(int)), this, SLOT(updateTimes()));
    connect(ui->dateEdit, SIGNAL(dateChanged(QDate)), this, SLOT(updateTimes()));
    updateTimes();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::updateTimes()
{
    double zen = 0;
    switch (ui->comboBox->currentIndex()) {
        case 0: zen = 90.8333333333333; break;
        case 1: zen = 96.0; break;
        case 2: zen = 102.0; break;
        case 3: zen = 108.0; break;
    }

    ui->label_6->setText(tr("Восход: ") + getSunTime(ui->dateEdit->date(),
                                    ui->doubleSpinBox->value(),
                                    ui->doubleSpinBox_2->value(),
                                    zen,
                                    ui->spinBox->value(),
                                    false).toString());
    ui->label_7->setText(tr("Закат: ") + getSunTime(ui->dateEdit->date(),
                                    ui->doubleSpinBox->value(),
                                    ui->doubleSpinBox_2->value(),
                                    zen,
                                    ui->spinBox->value(),
                                    true).toString());
}

QTime MainWindow::getSunTime(QDate dt, double latitude, double longitude, double zenith, qint8 localOffset, bool sunset)
{
    quint16 N;
    double LngHour;
    double M;
    double L;
    double RA;
    double Lquadrant;
    double RAquadrant;
    double sinDec;
    double cosDec;
    double HCos;
    double H;
    double LocalT;
    double UT;
    double t = 0;
    // 1. first calculate the day of the year
    N = dt.dayOfYear();
    // 2. convert the longitude to hour value and calculate an approximate time
    LngHour = longitude / 15;
    if (sunset) t = (double)N + (((double)18 - LngHour) / (double)24);
    else t = (double)N + (((double)6 - LngHour) / (double)24);
    // 3. calculate the Sun's mean anomaly
    M = ((double)0.9856 * t) - (double)3.289;
    // 4. calculate the Sun's true longitude
    double rd = (double)3.1415926535 / (double)180;
    double dg = (double)180 / (double)3.1415926535;
    L = M + ((double)1.916 * sin(M * rd)) + ((double)0.020 * sin((double)2 * M * rd)) + (double)282.634;
    while (L >= 360)  L = L - 360;
    while (L < 0) L = L + 360;
    // 5a. calculate the Sun's right ascension
    RA = atan((double)0.91764 * tan(L * rd)) * dg;
    while (RA >= 360)  RA = RA - 360;
    while (RA <= 360) RA = RA + 360;
    // 5b. right ascension value needs to be in the same quadrant as L
    Lquadrant = floor(L / 90) * 90;
    RAquadrant = floor(RA / 90) * 90;
    RA = RA + (Lquadrant - RAquadrant);
    // 5c. right ascension value needs to be converted into hours
    RA = RA / 15;
    // 6. calculate the Sun's declination
    sinDec = (double)0.39782 * sin(L * rd);
    cosDec = cos(asin(sinDec));
    // 7a. calculate the Sun's local hour angle
    HCos = (cos(zenith * rd) - (sinDec * sin(latitude * rd))) / (cosDec * cos(latitude * rd));
    if (HCos > 1 || HCos < -1) return QTime(0,0,59); //TODO
    // 7b. finish calculating H and convert into hours
    H = 0;
    if (sunset) H = acos(HCos) * dg;
    else H = (double)360 - acos(HCos) * dg;
    H = H / 15;
    // 8. calculate local mean time of rising/setting
    LocalT = H + RA - ((double)0.06571 * t) - (double)6.622;
    // 9. adjust back to UTC
    UT = LocalT - LngHour;
    while (UT >= 24)  UT = UT - 24;
    while (UT < 0) UT = UT + 24;
    // 10. convert UT value to local time zone of latitude/longitude
    t = UT + localOffset;
    while (t >= 24)  t = t - 24;
    while (t < 0) t = t + 24;
    quint8 hour = t;
    quint8 min = (t - hour) * 0.6 * 100;
    return QTime(hour, min, 0);
}
