#-------------------------------------------------
#
# Project created by QtCreator 2016-12-28T08:49:52
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SunRise
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

win32 {
        RC_FILE += applico.rc
        OTHER_FILES += applico.rc
}
